#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=20;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings

int file_length;

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    int valid = 0;
    // Hash the guess using MD5
    
    char *e = guess;
    while (*e && *e != '\r' && *e != '\n')
            e++;
    char *chunkyBoy = md5( guess, e - guess);

    //printf("Testing \"%s\" against \"%s\" -> \"%s\"\n", hash, guess, chunkyBoy);
    // Compare the two hashes
    if ( strcmp( hash, chunkyBoy ) == 0 ){
        printf( "%s %s\n", chunkyBoy, guess);
        valid = 1;
    }

    // Free any malloc'd memory
    free( chunkyBoy );
    
    return valid;
}

// Read in a file and return the array of strings.
// Use the technique we showed in class to expand the
// array as you go.
char *readfile(char *filename)
{
    // Get size of file
    struct stat info;
    stat(filename, &info);
    file_length = info.st_size;
    
    // Allocate space for the entire file
    char *chunk = (char *)malloc(file_length+1);
    
    // Read in entire file
    FILE *f = fopen(filename, "r");
    if (!f)
    {
        perror("Can't open file");
        exit(1);
    }
    fread(chunk, 1, file_length, f);
    fclose(f);
    
    // Add null character to end
    chunk[file_length] = '\0';
    
    return chunk;
}

char **parsefile(char *chunky)
{
    // Change newlines to nulls, counting as we go
    int lines = 0;
    for (int i = 0; i < file_length; i++)
    {
        if (chunky[i] == '\n') 
        {
            chunky[i] = '\0';
            lines++;
        }
    }
    
    // If the file didn't have a final terminating newline,
    // it won't be counted. Let's check for that situation
    // and adjust the linecount accordingly. Since we've already
    // changed newlines to null, all we need to do is look
    // for a null character at the end of the chunk.
    if (chunky[file_length-1] != '\0') lines++;
    
    // Allocate space for an array of pointers
    char **strings = malloc((lines+1) * sizeof(char *));
    
    // Loop through chunk, storing pointers to each string
    int string_idx = 0;
    for (int i = 0; i < file_length; i += strlen(chunky+i) + 1)
    {
        strings[string_idx] = chunky+i;
        string_idx++;
    }
    
    // Store terminating NULL
    strings[lines] = NULL;
    
    return strings;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // Read the hash file into an array of strings
    char *readhashes = readfile(argv[1]);
    char **hashes = parsefile(readhashes);

    // Read the dictionary file into an array of strings
    char *readdict = readfile(argv[2]);
    char **dict = parsefile(readdict);

    // For each hash, try every entry in the dictionary.
    // Print the matching dictionary entry.
    // Need two nested loops.
    
    for( int i = 0; hashes[i] != NULL; i++)
    {
        printf( "%d\n", i);
        for (int j = 0; dict[j] != NULL; j++)
        {
            printf("%d\n", j);
            if (tryguess( hashes[i], dict[j] ))
                break; //optimization
        }
    }
    
    free(readhashes);
    free(hashes);
    free(readdict);
    free(dict);
}
